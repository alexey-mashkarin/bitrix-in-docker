# Set defaults.
defaults

# Enable or disable TLS/SSL encryption.
tls on
tls_starttls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
tls_certcheck off

# Set up a default account's settings.
account default
host ${MAIL_HOST}
port ${MAIL_PORT}
auth on
user ${MAIL_USERNAME}
password ${MAIL_PASSWORD}
from ${MAIL_FROM}
syslog LOG_MAIL