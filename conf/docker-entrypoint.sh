#!/bin/bash

# Map host user id with nginx user in docker container
if [ -n "$PUID" ]; then
  if [ -z "$PGID" ]; then
    PGID=${PUID}
  fi
  sed -i -e 's/nginx:x:[0-9]*:/nginx:x:'$PGID':/g' /etc/group
  sed -i -e 's/nginx:x:[0-9]*:[0-9]*:/nginx:x:'$PUID':'$PGID':/g' /etc/passwd
  chown -R nginx:nginx /var/www/html
fi


echo "Run /usr/local/bin/mysql-entrypoint.sh"
/usr/local/bin/mysql-entrypoint.sh mysqld
echo "Mysql prepared"


# Set env vars for cron jobs
printenv > /etc/environment


if [ -n "${CERT_NAME}" ] && [ -f "/etc/nginx/certs/${CERT_NAME}.crt" ] && [ -f "/etc/nginx/certs/${CERT_NAME}.key" ]; then
  envsubst '${CERT_NAME}' < /etc/nginx/templates/site-default-ssl.tpl.conf > /etc/nginx/conf.d/site-default-ssl.conf
fi


if [ -n "${MAIL_HOST}" ] && [ -n "${MAIL_PORT}" ] && [ -n "${MAIL_USERNAME}" ] && [ -n "${MAIL_PASSWORD}" ] && [ -n "${MAIL_FROM}" ]; then
  envsubst '${MAIL_HOST} ${MAIL_PORT} ${MAIL_USERNAME} ${MAIL_PASSWORD} ${MAIL_FROM}' < /etc/msmtprc.tpl > /etc/msmtprc
fi


if [ -n "$VIRTUAL_HOST" ]; then
  echo $VIRTUAL_HOST > /etc/hostname
  echo "
127.0.0.1 $VIRTUAL_HOST
" >> /etc/hosts
fi


if [ "$APP_ENV" == "production" ] ; then
  cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
  echo "xdebug.mode=off" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
else
  if [ -n "$XDEBUG_MODE" ] ; then
    echo "xdebug.mode=$XDEBUG_MODE" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini ;
  fi
  cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
fi


# Default docker-php-entrypoint
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

echo "Run command:" && echo "$@"
exec "$@"