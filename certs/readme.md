## Генерация сертификата

```
openssl req -x509 -out default.crt -keyout default.key -days 365 \
-newkey rsa:2048 -nodes -sha256 \
-subj '/CN=*.bitrix.loc' -extensions EXT -config <( \
printf "[dn]\nCN=*.bitrix.loc\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:*.bitrix.loc\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```
