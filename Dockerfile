FROM php:7.4.28-fpm-buster

ENV TZ Europe/Moscow

RUN set -x \
&& apt-get update \
&& apt-get install -y tzdata \
&& cp /usr/share/zoneinfo/$TZ /etc/localtime \
&& echo $TZ > /etc/timezone


#################
## Build Nginx ##
#################

RUN set -eux \
&& RUN_DEPS=" \
    libgeoip-dev \
"; \
BUILD_DEPS=" \
    build-essential \
    libpcre3-dev \
    zlib1g-dev \
    libssl-dev \
"; \
apt-get update && apt install -y \
    $(echo "${BUILD_DEPS}") \
    $(echo "${RUN_DEPS}") \
&& NGINX_VERSION="1.21.4"; \
BUILD_DIR="/usr/src"; \
cd ${BUILD_DIR} \
&& curl -fSLO http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && tar zxf nginx-${NGINX_VERSION}.tar.gz \
&& rm -rf *.tar.gz \
&& cd ${BUILD_DIR}/nginx-${NGINX_VERSION} \
&& ./configure \
    --prefix=/etc/nginx \
    --sbin-path=/usr/sbin/nginx \
    --modules-path=/usr/lib/nginx/modules \
    --conf-path=/etc/nginx/nginx.conf \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --pid-path=/var/run/nginx.pid \
    --lock-path=/var/run/nginx.lock \
    --http-client-body-temp-path=/var/cache/nginx/client_temp \
    --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
    --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
    --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
    --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
    --with-perl_modules_path=/usr/lib/perl5/vendor_perl \
    --user=nginx \
    --group=nginx \
    --with-compat \
    --with-file-aio \
    --with-threads \
    --with-http_addition_module \
    --with-http_auth_request_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_geoip_module \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_mp4_module \
    --with-http_random_index_module \
    --with-http_realip_module \
    --with-http_secure_link_module \
    --with-http_slice_module \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_sub_module \
    --with-http_v2_module \
    --with-mail \
    --with-mail_ssl_module \
    --with-stream \
    --with-stream_geoip_module \
    --with-stream_realip_module \
    --with-stream_ssl_module \
    --with-stream_ssl_preread_module \
    --with-cc-opt='-Os -fomit-frame-pointer -g' \
    --with-ld-opt=-Wl,--as-needed,-O1,--sort-common \
&& make \
&& make install \
&& apt-get remove -y $(echo "${BUILD_DEPS}") \
&& rm -rf ${BUILD_DIR}/nginx-${NGINX_VERSION} \
&& ln -s /usr/lib/nginx/modules /etc/nginx/modules \
&& nginx -V \
&& adduser --system --home /nonexistent --shell /bin/false --no-create-home --disabled-login --disabled-password --gecos "nginx user" --group nginx \
&& mkdir -p /var/cache/nginx/client_temp \
/var/cache/nginx/fastcgi_temp \
/var/cache/nginx/proxy_temp \
/var/cache/nginx/scgi_temp \
/var/cache/nginx/uwsgi_temp \
/etc/nginx/conf.d \
/etc/nginx/snippets \
/etc/nginx/ssl \
/etc/nginx/templates \
/run/nginx \
&& chmod 700 /var/cache/nginx/* \
&& chown nginx:nginx /var/cache/nginx/* \
&& nginx -t \
&& chmod -R 640 /var/log/nginx \
&& rm /var/log/nginx/access.log /var/log/nginx/error.log \
&& ln -s /dev/stdout /var/log/nginx/access.log \
&& ln -s /dev/stdout /var/log/nginx/error.log \
&& chown -R nginx:nginx /var/log/nginx

COPY ./conf/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./conf/nginx/server.conf /etc/nginx/server.conf
COPY ./conf/nginx/site-default.conf /etc/nginx/conf.d/site-default.conf
COPY ./conf/nginx/site-default-ssl.tpl.conf /etc/nginx/templates/site-default-ssl.tpl.conf

EXPOSE 80 443

######################
###  Configure PHP  ##
######################

ENV APP_ENV production
COPY ./conf/php/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY ./conf/php/php-fpm.d/www.conf /usr/local/etc/php-fpm.d/www.conf
COPY ./conf/php/conf.d /usr/local/etc/php/conf.d


RUN set -eux \
&& rm -Rf /var/www/* \
&& mkdir -p /var/www/html/ \
&& touch /var/run/php-fpm.sock \
&& chown nginx:nginx /var/www/html \
&& chown nginx:nginx /var/run/php-fpm.sock \
&& chmod 0666 /var/run/php-fpm.sock \
&& RUN_DEPS=" \
    libfreetype6-dev \
    libicu-dev \
    libjpeg-dev \
    libpng-dev \
    libsqlite3-dev \
    libxslt-dev \
    libzip-dev \
"; \
apt update && apt install -y $(echo "${RUN_DEPS}") \
&& docker-php-ext-configure gd \
      --enable-gd \
      --with-freetype \
      --with-jpeg \
&& docker-php-ext-install iconv pdo_mysql pdo_sqlite mysqli gd exif intl xsl json soap dom zip opcache \
&& pecl install xdebug-3.1.3 \
&& docker-php-ext-enable xdebug \
&& { \
  echo "xdebug.client_host=host.docker.internal"; \
  echo "xdebug.start_with_request=yes"; \
} | tee --append /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
&& docker-php-source delete


RUN set -eux \
&& EXPECTED_COMPOSER_SIGNATURE=$(curl https://composer.github.io/installer.sig) \
&& php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
&& php -r "if (hash_file('SHA384', 'composer-setup.php') === '${EXPECTED_COMPOSER_SIGNATURE}') { echo 'Composer.phar Installer verified'; } else { echo 'Composer.phar Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"  \
&& php composer-setup.php --install-dir=/usr/bin --filename=composer \
&& php -r "unlink('composer-setup.php');"


####################
### Install MySql ##
####################

RUN set -eux \
&& groupadd -r mysql && useradd -r -g mysql mysql \
&& INSTALL_DEPS=" \
    gnupg \
    dirmngr \
    pwgen \
    xz-utils \
" \
&& apt-get update && apt-get install -y --no-install-recommends $(echo "${INSTALL_DEPS}") \
#&& rm -rf /var/lib/apt/lists/* \
&& \
GOSU_VERSION="1.12"; \
MYSQL_MAJOR="5.7"; \
MYSQL_VERSION="5.7.*"; \
savedAptMark="$(apt-mark showmanual)"; dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
curl -fSL -o /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-${dpkgArch}"; \
curl -fSL -o /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-${dpkgArch}.asc"; \
export GNUPGHOME="$(mktemp -d)"; \
gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
gpgconf --kill all;  rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc; \
apt-mark auto '.*' > /dev/null; \
[ -z "$savedAptMark" ] || apt-mark manual $savedAptMark > /dev/null; \
apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
chmod +x /usr/local/bin/gosu; \
gosu --version; \
gosu nobody true; \
key='3A79BD29'; \
export GNUPGHOME="$(mktemp -d)"; \
gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key"; \
gpg --batch --export "$key" > /etc/apt/trusted.gpg.d/mysql.gpg; \
gpgconf --kill all; \
rm -rf "$GNUPGHOME"; \
apt-key list > /dev/null \
&& echo 'deb http://repo.mysql.com/apt/debian/ buster mysql-5.7' > /etc/apt/sources.list.d/mysql.list \
&& { \
    echo mysql-community-server mysql-community-server/data-dir select ''; \
    echo mysql-community-server mysql-community-server/root-pass password ''; \
    echo mysql-community-server mysql-community-server/re-root-pass password ''; \
    echo mysql-community-server mysql-community-server/remove-test-db select false; \
} | debconf-set-selections \
&& apt-get update && apt-get install -y \
mysql-server="${MYSQL_VERSION}" \
&& find /etc/mysql/ -name '*.cnf' -print0   | xargs -0 grep -lZE '^(bind-address|log)'   | xargs -rt -0 sed -Ei 's/^(bind-address|log)/#&/' \
&& echo '[mysqld]\nskip-host-cache\nskip-name-resolve' > /etc/mysql/conf.d/docker.cnf \
&& rm -rf /var/lib/apt/lists/* \
&& apt-get remove -y $(echo "${INSTALL_DEPS}") \
&& rm -rf /var/lib/mysql \
&& mkdir -p /var/lib/mysql /var/run/mysqld /docker-entrypoint-initdb.d \
&& chown -R mysql:mysql /var/lib/mysql /var/run/mysqld \
&& chmod 777 /var/run/mysqld /var/lib/mysql

COPY conf/mysql/mysql.cnf /etc/mysql/conf.d/mysql.cnf

EXPOSE 3306 33060

# Install mail transfer agent
RUN set -eux \
&& apt-get update && apt-get install -y msmtp-mta ca-certificates
COPY conf/smtp/msmtprc.tpl /etc/msmtprc.tpl


## Set supervisord configuration
RUN set -eux \
&& apt-get update && apt-get install -y supervisor \
&& mkdir -p /var/log/supervisor \
&& mkdir -p /etc/supervisor/conf.d/

COPY conf/supervisor /etc/supervisor/


# Cron job for Bitrix
COPY conf/cron/nginx /var/spool/cron/crontabs/nginx
RUN set -eux \
&& chown nginx:nginx /var/spool/cron/crontabs/nginx && chmod 600 /var/spool/cron/crontabs/nginx


COPY conf/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
COPY conf/mysql-entrypoint.sh /usr/local/bin/mysql-entrypoint.sh
RUN set -eux \
&& apt-get update && apt-get install -y \
    gettext-base \
    procps \
    cron \
&& chmod 755 /usr/local/bin/mysql-entrypoint.sh \
&& chmod 755 /usr/local/bin/docker-entrypoint.sh \
&& chown -R nginx:crontab /var/www/html


STOPSIGNAL SIGQUIT

WORKDIR "/var/www/html"

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

# Run supervisor for running nginx and php-fpm
CMD ["/usr/bin/supervisord","-n","-c","/etc/supervisor/supervisord.conf"]

# chek nginx, php-fpm, mysqld proces running
HEALTHCHECK --start-period=5s --interval=60s --timeout=1s --retries=3 \
  CMD ps -C nginx || exit 1 && ps -C php-fpm || exit 1 && ps -C mysqld || exit 1