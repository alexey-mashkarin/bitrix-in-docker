# bitrix-shop

## Как использовать

Необходимо создать файл .env из .env.example, прописать корректный VIRTUAL_HOST, добавить значение VIRTUAL_HOST в hosts (для локального запуска)

После запуска подключиться к контейнеру и запустить в папке /var/www/html команду
```shell
curl -fSLO http://www.1c-bitrix.ru/download/scripts/bitrixsetup.php
```

Затем открыть в браузере ссылку https://$VIRTUAL_HOST/bitrixsetup.php, выполнить установку продукта

## bitrix_server_test.php

```shell
curl -fSLO https://dev.1c-bitrix.ru/download/scripts/bitrix_server_test.php
```